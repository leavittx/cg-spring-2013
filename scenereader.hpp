#pragma once

#include "scene.hpp"
#include "camera.hpp"
#include "light.hpp"
#include "sphere.hpp"
#include "plane.hpp"
#include "triangle.hpp"
#include "cylinder.hpp"
#include "cone.hpp"
#include "mesh.hpp"
#include "csgtree.hpp"
#include "csgops.hpp"

#include <QPair>
#include <QString>
#include <QDomElement>
#include <QSharedPointer>

class SceneReader
{
public:
	SceneReader() { }
	virtual ~SceneReader() { }

	bool read(const QString &path, QSharedPointer<Scene> scene, QSharedPointer<Camera> camera);

private:
	bool process_camera(QDomElement &node, QSharedPointer<Camera> camera);
	bool process_light(QDomElement &node, Light *light);
	bool process_material(QDomElement &node, Material * const material);
	bool process_sphere(QDomElement &node, Sphere * &shape);
	bool process_plane(QDomElement &node, Plane * &shape);
	bool process_triangle(QDomElement &node, Triangle * &shape);
	bool process_cylinder(QDomElement &node, Cylinder * &shape);
	bool process_cone(QDomElement &node, Cone * &shape);
	bool process_mesh(QDomElement &node, Mesh * &shape);

	bool process_csg(QDomElement &node, CSG::Tree * &tree);
	bool process_operation(QDomElement &node, CSG::Node * &op);
	bool process_operand(QDomElement &node, CSG::Node * &operand);
};