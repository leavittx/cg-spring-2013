#include "triangle.hpp"

using namespace glm;

RayIntersection Triangle::findIntersection(const Ray &ray) const
{
		vec3 u, v, n;              // triangle vectors
		vec3 dir, w0, w;           // ray vectors
		vec3 pointOfIntersection;
		float r, a, b;              // params to calc ray-plane intersect

		// get triangle edge vectors and plane normal
		u = mPointB - mPointA;
		v = mPointC - mPointA;
		n = cross(u, v);              // cross product
		if (n == vec3(0))             // triangle is degenerate
			return RayIntersection(false);                 // do not deal with this case

		dir = ray.direction();              // ray direction vector
		w0 = ray.origin() - mPointA;
		a = -dot(n, w0);
		b = dot(n, dir);
		if (abs(b) < EPS) {     // ray is  parallel to triangle plane
			if (abs(a) < EPS)                 // ray lies in triangle plane
				return RayIntersection(false);
			else return RayIntersection(false);              // ray disjoint from plane
		}

		// get intersect point of ray with triangle plane
		r = a / b;
		if (r < 0.0)                    // ray goes away from triangle
			return 0;                   // => no intersect
		// for a segment, also test if (r > 1.0) => no intersect


		pointOfIntersection = ray.origin() + r * dir;            // intersect point of ray and plane

		// is I inside T?
		float    uu, uv, vv, wu, wv, D;
		uu = dot(u,u);
		uv = dot(u,v);
		vv = dot(v,v);
		w = pointOfIntersection - mPointA;
		wu = dot(w,u);
		wv = dot(w,v);
		D = uv * uv - uu * vv;

		// get and test parametric coords
		float s, t;
		s = (uv * wv - vv * wu) / D;
		if (s < 0.0 || s > 1.0)         // I is outside T
			return RayIntersection(false);
		t = (uv * wu - uu * wv) / D;
		if (t < 0.0 || (s + t) > 1.0)  // I is outside T
			return RayIntersection(false);

		// Cache s and t values for normals smoothing
		mS = s;
		mT = t;

		// I is in T
		RayIntersection result(true);
		result.distance = r;
		result.hitShape = this;
		result.normal = getNormal(ray, result.distance);

		return result;
}

glm::vec3 Triangle::getNormal(const Ray &ray, float distance) const
{
	return mNormal;
}

RayIntersection TriangleSmoothed::findIntersection(const Ray &ray) const
{
	RayIntersection result = Triangle::findIntersection(ray);

	// Smooth the normal between the vertices
	result.normal = normalize(mS * mNormalB + mT * mNormalC + (1 - mS - mT) * mNormalA);

	return result;
}