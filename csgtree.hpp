#pragma once

#include "shape.hpp"
#include "csgnode.hpp"

namespace CSG {

class Tree : public BasicShape
{
public:
	Tree(Node *root = nullptr, Material *material = new Material())
		: BasicShape(material), mRoot(root) { }
	virtual ~Tree();

	RayIntersection findIntersection(const Ray &ray) const;
	glm::vec3 getNormal(const Ray &ray, float distance) const;

	void setRoot(Node *node) { mRoot = node; }

private:
	Node *mRoot;
};

} // namespace CSG