#include "scenereader.hpp"

#include "objreader.hpp"

#include <QFile>
#include <QDomDocument>
#include <QMessageBox>
#include <qdebug.h>

#include <iostream>

using namespace std;
using namespace glm;

bool read_vec3(const QDomElement &node, vec3 &v)
{
	bool ok;

	if (node.hasAttribute("x"))
		v.x = node.attribute("x").toFloat(&ok);
	if (! ok) return false;

	if (node.hasAttribute("y"))
		v.y = node.attribute("y").toFloat(&ok);
	if (! ok) return false;

	if (node.hasAttribute("z"))
		v.z = node.attribute("z").toFloat(&ok);
	if (! ok) return false;

	return true;
}

bool read_f32(const QDomElement &node, const QString &attr, float &x)
{
	bool ok;

	if (node.hasAttribute(attr))
		x = node.attribute(attr).toFloat(&ok);
	if (! ok) return false;

	return true;
}

bool SceneReader::process_camera(QDomElement &node, QSharedPointer<Camera> camera)
{
	vec3 pos, up, look_at;
	float fov, dist_to_near_plane;

	while (! node.isNull())
	{
		if (node.tagName() == "pos") {
			if (! read_vec3(node, pos))
				return false;
		}
		else if (node.tagName() == "up") {
			if (! read_vec3(node, up))
				return false;
		}
		else if (node.tagName() == "look_at") {
			if (! read_vec3(node, look_at))
				return false;
		}
		else if (node.tagName() == "fov") {
			if (! read_f32(node, "angle", fov))
				return false;
		}
		else if (node.tagName() == "dist_to_near_plane") {
			if (! read_f32(node, "dist", dist_to_near_plane))
				return false;
		}

		node = node.nextSiblingElement();
	}

	camera->lookAt(pos, up, look_at);
	camera->setFoV(fov);
	camera->setNearPlane(dist_to_near_plane);

	return true;
}

bool read_attenuation(const QDomElement &node, vec3 &att)
{
	bool ok;

	if (node.hasAttribute("const"))
		att.x = node.attribute("const").toFloat(&ok);
	if (! ok) return false;

	if (node.hasAttribute("linear"))
		att.y = node.attribute("linear").toFloat(&ok);
	if (! ok) return false;

	if (node.hasAttribute("quad"))
		att.z = node.attribute("quad").toFloat(&ok);
	if (! ok) return false;

	return true;
}

bool SceneReader::process_light(QDomElement &node, Light *light)
{
	vec3 pos, dir;
	vec3 ambient_emission, diffuse_emission, specular_emission;
	float inner_cutoff, outer_cutoff, spot_exponent;
	vec3 att = vec3(0.500, 0.00002, 0.00008);

	while (! node.isNull())
	{
		if (node.tagName() == "pos") {
			if (! read_vec3(node, pos))
				return false;
		}
		else if (node.tagName() == "dir") {
			if (! read_vec3(node, dir))
				return false;
		}
		else if (node.tagName() == "ambient_emission") {
			if (! read_vec3(node, ambient_emission))
				return false;
		}
		else if (node.tagName() == "diffuse_emission") {
			if (! read_vec3(node, diffuse_emission))
				return false;
		}
		else if (node.tagName() == "specular_emission") {
			if (! read_vec3(node, specular_emission))
				return false;
		}
		else if (node.tagName() == "inner_cutoff" || node.tagName() == "umbra") {
			if (! read_f32(node, "angle", inner_cutoff))
				return false;
		}
		else if (node.tagName() == "outer_cutoff" || node.tagName() == "penumbra") {
			if (! read_f32(node, "angle", outer_cutoff))
				return false;
		}
		else if (node.tagName() == "falloff") {
			if (! read_f32(node, "value", spot_exponent))
				return false;
		}
		else if (node.tagName() == "attenuation") {
			if (! read_attenuation(node, att))
				return false;
		}

		node = node.nextSiblingElement();
	}

	light->setPosition(pos);
	light->setDirection(dir);
	light->setColor(ColorDesc(ambient_emission, diffuse_emission, specular_emission));
	light->setInnerCutoff(inner_cutoff);
	light->setOuterCutoff(outer_cutoff);
	light->setExponent(spot_exponent);
	light->setAttenuation(att);

	return true;
}

bool read_illumination_factors(const QDomElement &node, vec3 &factors)
{
	bool ok;

	if (node.hasAttribute("illumination_factor"))
		factors.x = node.attribute("illumination_factor").toFloat(&ok);
	if (! ok) return false;

	if (node.hasAttribute("reflection_factor"))
		factors.y = node.attribute("reflection_factor").toFloat(&ok);
	if (! ok) return false;

	if (node.hasAttribute("refraction_factor"))
		factors.z = node.attribute("refraction_factor").toFloat(&ok);
	if (! ok) return false;

	return true;
}

bool SceneReader::process_material(QDomElement &node, Material * const material)
{
	vec3 ambient, diffuse, specular;
	float specular_power, refraction_coeff;
	float illumination_factor, reflection_factor, refraction_factor;

	while (! node.isNull())
	{
		if (node.tagName() == "ambient") {
			if (! read_vec3(node, ambient))
				return false;
		}
		else if (node.tagName() == "diffuse") {
			if (! read_vec3(node, diffuse))
				return false;
		}
		else if (node.tagName() == "specular") {
			if (! read_vec3(node, specular))
				return false;
		}
		else if (node.tagName() == "specular_power") {
			if (! read_f32(node, "power", specular_power))
				return false;
		}
		else if (node.tagName() == "refraction_coeff") {
			if (! read_f32(node, "theta", refraction_coeff))
				return false;
		}
		else if (node.tagName() == "illumination_factors") {
			vec3 illumination_factors;
			if (! read_illumination_factors(node, illumination_factors))
				return false;
			illumination_factor = illumination_factors.x;
			reflection_factor		= illumination_factors.y;
			refraction_factor		= illumination_factors.z;
		}

		node = node.nextSiblingElement();
	}

	material->color = ColorDesc(ambient, diffuse, specular, specular_power);
	material->refractionIndex = refraction_coeff;
	material->illumination = illumination_factor;
	material->reflection = reflection_factor;
	material->refraction = refraction_factor;

	return true;
}


bool SceneReader::process_sphere(QDomElement &node, Sphere * &shape)
{
	vec3 center;
	float radius;
	Material *material = new Material;

	while (! node.isNull())
	{
		if (node.tagName() == "center") {
			if (! read_vec3(node, center))
				return false;
		}
		else if (node.tagName() == "radius") {
			if (! read_f32(node, "r", radius))
				return false;
		}
		else if (node.tagName() == "material") {
			QDomElement material_node = node.firstChildElement();
			if (! process_material(material_node, material))
				return false;
		}

		node = node.nextSiblingElement();
	}

	shape = new Sphere(center, radius, material);

	return true;
}

bool SceneReader::process_plane(QDomElement &node, Plane * &shape)
{
	vec3 normal;
	float distance;
	Material *material = new Material;

	while (! node.isNull())
	{
		if (node.tagName() == "normal") {
			if (! read_vec3(node, normal))
				return false;
		}
		else if (node.tagName() == "D") {
			if (! read_f32(node, "d", distance))
				return false;
		}
		else if (node.tagName() == "material") {
			QDomElement material_node = node.firstChildElement();
			if (! process_material(material_node, material))
				return false;
		}

		node = node.nextSiblingElement();
	}

	shape = new Plane(normal, distance, material);

	return true;
}

bool SceneReader::process_triangle(QDomElement &node, Triangle * &shape)
{
	vec3 points[3];
	Material *material = new Material;
	int readPoints = 0;

	while (! node.isNull())
	{
		if (node.tagName() == "pos") {
			if (readPoints > 2)
				return false;

			if (! read_vec3(node, points[readPoints]))
				return false;

			readPoints++;	
		}
		else if (node.tagName() == "material") {
			QDomElement material_node = node.firstChildElement();
			if (! process_material(material_node, material))
				return false;
		}

		node = node.nextSiblingElement();
	}

	shape = new Triangle(points[0], points[1], points[2], material);

	return true;
}

bool SceneReader::process_cylinder(QDomElement &node, Cylinder * &shape)
{
	vec3 topC, bottomC;
	float r;
	Material *material = new Material;

	while (! node.isNull())
	{
		if (node.tagName() == "bottom") {
			if (! read_vec3(node, bottomC))
				return false;
		}
		else if (node.tagName() == "top") {
			if (! read_vec3(node, topC))
				return false;
		}		
		else if (node.tagName() == "radius") {
			if (! read_f32(node, "r", r))
				return false;
		}
		else if (node.tagName() == "material") {
			QDomElement material_node = node.firstChildElement();
			if (! process_material(material_node, material))
				return false;
		}

		node = node.nextSiblingElement();
	}

	//bottomC.z = topC.z;

	shape = new Cylinder(bottomC, topC, r, material);

	return true;
}

bool SceneReader::process_cone(QDomElement &node, Cone * &shape)
{
	vec3 topC, bottomC;
	float r;
	Material *material = new Material;

	while (! node.isNull())
	{
		if (node.tagName() == "bottom") {
			if (! read_vec3(node, bottomC))
				return false;
		}
		else if (node.tagName() == "top") {
			if (! read_vec3(node, topC))
				return false;
		}		
		else if (node.tagName() == "radius") {
			if (! read_f32(node, "r", r))
				return false;
		}
		else if (node.tagName() == "material") {
			QDomElement material_node = node.firstChildElement();
			if (! process_material(material_node, material))
				return false;
		}

		node = node.nextSiblingElement();
	}

	shape = new Cone(topC, bottomC, r, material);

	return true;
}

bool SceneReader::process_mesh(QDomElement &node, Mesh * &shape)
{
	vec3 translation, scale;
	QString filename;
	Material *material = new Material;

	while (! node.isNull())
	{
		if (node.tagName() == "translation" || node.tagName() == "translate") {
			if (! read_vec3(node, translation))
				return false;
		}
		else if (node.tagName() == "scale") {
			if (! read_vec3(node, scale))
				return false;
		}		
		else if (node.tagName() == "model") {
			if (! node.hasAttribute("file_name"))
				return false;
			else
				filename = node.attribute("file_name");
		}
		else if (node.tagName() == "material") {
			QDomElement material_node = node.firstChildElement();
			if (! process_material(material_node, material))
				return false;
		}

		node = node.nextSiblingElement();
	}

	MeshBB res;

	ObjReader reader;
	if (! reader.read(res, filename, translation, scale))
		return false;

	shape = new Mesh(res.first, res.second, material);

	return true;
}

bool SceneReader::process_operand(QDomElement &node, CSG::Node * &operand)
{
	if (node.tagName() == "object")
	{
		QString type;

		if (node.hasAttribute("type")) {
			type = node.attribute("type");
		}
		else {
			qDebug() << "No type attribute specified for object.";
			return false;
		}

		node = node.firstChildElement();
		
		if (type == "sphere") {
			Sphere *shape;
			process_sphere(node, shape);
			operand = new CSG::LeafNode(shape);
		}
		else if (type == "plane") {
			Plane *shape;
			process_plane(node, shape);
			operand = new CSG::LeafNode(shape);
		}
		else if (type == "triangle") {
			Triangle *shape;
			process_triangle(node, shape);
			operand = new CSG::LeafNode(shape);
		}
		else if (type == "cylinder") {
			Cylinder *shape;
			process_cylinder(node, shape);
			operand = new CSG::LeafNode(shape);
		}
		else if (type == "cone") {
			Cone *shape;
			process_cone(node, shape);
			operand = new CSG::LeafNode(shape);
		}
		else if (type == "mesh" || type == "model") {
			Mesh *shape;
			if (! process_mesh(node, shape))
				return false;
			operand = new CSG::LeafNode(shape);
		}
	}
	else if (node.tagName() == "operation")
	{
		if (! process_operation(node, operand))
			return false;
	}

	return true;
}

bool SceneReader::process_operation(QDomElement &node, CSG::Node * &op)
{
	QString type;
	CSG::Node *left, *right;

	if (node.hasAttribute("type")) {
		type = node.attribute("type");
	}
	else {
		qDebug() << "No type attribute specified for CSG operation.";
		return false;
	}

	node = node.firstChildElement();

	while (! node.isNull())
	{
		if (node.tagName() == "left")
		{
			if (! process_operand(node.firstChildElement(), left))
				return false;
		}
		if (node.tagName() == "right")
		{
			if (! process_operand(node.firstChildElement(), right))
				return false;
		}

		node = node.nextSiblingElement();
	}

	if (type == "union" || type == "unite")
		op = new CSG::Union(left, right);
	else if (type == "diff" || type == "difference")
		op = new CSG::Difference(left, right);
	else if (type == "isect" || type == "intersection")
		op = new CSG::Intersection(left, right);
	else {
		qDebug() << "Invalid CSG operation " << type;
		qDebug() << "Supported operations: union, difference, intersection";
		return false;
	}

	return true;
}

bool SceneReader::process_csg(QDomElement &node, CSG::Tree * &tree)
{
	while (! node.isNull())
	{
		if (node.tagName() == "operation")
		{
			CSG::Node *op;
			if (! process_operation(node, op))
				return false;
			tree = new CSG::Tree(op);
		}

		node = node.nextSiblingElement();
	}

	return true;
}


bool SceneReader::read(const QString &path, QSharedPointer<Scene> scene, QSharedPointer<Camera> camera)
{
	QFile file(path);
	if (! file.exists())
	{
		qDebug() << path << " does not exist.";
		return false;
	}

	qDebug() << "Loading scene " << path;

	QString errorStr;
	int errorLine, errorColumn;
	QDomDocument domDocument;
	domDocument.setContent(&file, true, &errorStr, &errorLine, &errorColumn);

	if (! errorStr.isEmpty())
	{
		QString errorMsg("XML parsing error: on line " + QString::number(errorLine) + ", column " + QString::number(errorColumn) + " : " + errorStr);
		qDebug() << errorMsg;
		//QMessageBox::critical(0, QString("Ray tracing"), errorMsg, QMessageBox::Abort);
		return false;
	}

	QDomElement sceneXML = domDocument.documentElement();

	QDomElement elem = sceneXML.firstChildElement();
	while (! elem.isNull())
	{
		if (elem.tagName() == "camera") {
			QDomElement node = elem.firstChildElement();
			process_camera(node, camera);
		}
		else if (elem.tagName() == "light") {
			Light *light = new Light;

			if (elem.hasAttribute("type")) {
				QString type = elem.attribute("type");
				if (type == "point")
					light->setType(LIGHT_POINT);
				else if (type == "directional")
					light->setType(LIGHT_DIRECTIONAL);
				else if (type == "spotlight")
					light->setType(LIGHT_SPOT);
			}

			QDomElement node = elem.firstChildElement();
			process_light(node, light);
			scene->addLight(light);
		}
		else if (elem.tagName() == "object") {
			QDomElement node = elem.firstChildElement();

			if (elem.hasAttribute("type")) {
				QString type = elem.attribute("type");
				if (type == "sphere") {
					Sphere *shape;
					process_sphere(node, shape);
					scene->addObject(shape);
				}
				else if (type == "plane") {
					Plane *shape;
					process_plane(node, shape);
					scene->addObject(shape);
				}
				else if (type == "triangle") {
					Triangle *shape;
					process_triangle(node, shape);
					scene->addObject(shape);
				}
				else if (type == "cylinder") {
					Cylinder *shape;
					process_cylinder(node, shape);
					scene->addObject(shape);
				}
				else if (type == "cone") {
					Cone *shape;
					process_cone(node, shape);
					scene->addObject(shape);
				}
				else if (type == "mesh" || type == "model") {
					Mesh *shape;
					if (! process_mesh(node, shape))
						return false;
					scene->addObject(shape);
				}
				else if (type == "CSG") {
					CSG::Tree *root;
					if (! process_csg(node, root))
						return false;
					scene->addObject(root);
				}
			}
		}
		else if (elem.tagName() == "global" || elem.tagName() == "background") {
			Material *air = new Material;
			if (! process_material(elem.firstChildElement().firstChildElement(), air))
				qDebug() << "Unable to process global material block.";

			air->refraction = 1.f;
			air->reflection = 0.f;
			air->refractionIndex = 1.f;
			scene->setAir(air);
		}

		elem = elem.nextSiblingElement();

		//else if (document.tagName() == "include") {
		//	load(document.attribute("url").toStdString());
		//}
	}

	file.close();

	return true;
}