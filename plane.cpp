#include "plane.hpp"

using namespace glm;

RayIntersection Plane::findIntersection(const Ray &ray) const
{
	float RdotN = glm::dot(ray.direction(), mNormal);
	float distance = -(glm::dot(mNormal, ray.origin()) + mDistance) / RdotN;

	if (abs(RdotN) < 1e-5 || distance <= 0)
		return RayIntersection(false);

	RayIntersection result(true);
	result.distance = distance;
	result.hitShape = this;
	result.normal = getNormal(ray, result.distance);

	return result;
}

glm::vec3 Plane::getNormal(const Ray &ray, float distance) const
{
	return mNormal;
}