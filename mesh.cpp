#include "mesh.hpp"

#include <QCoreApplication>

using namespace glm;

bool AABB::intersects(const Ray &ray) const
{
	const vec3 &origin    = ray.origin(), &direction = ray.direction();
	float d0 = -INFINITY, d1 = INFINITY;

	if (abs(direction.x) > EPS)
	{
		d0 = (min.x - origin.x) / direction.x;
		d1 = (max.x - origin.x) / direction.x;

		if (d1 < d0)
			std::swap(d0, d1);
	}

	if (abs(direction.y) > EPS)
	{
		float t0, t1;
		t0 = (min.y - origin.y) / direction.y;
		t1 = (max.y - origin.y) / direction.y;

		if (t1 < t0)
			std::swap(t0, t1);

		d0 = std::max(d0,t0);
		d1 = std::min(d1,t1);
	}

	if (abs(direction.z) > EPS)
	{
		float t0, t1;
		t0 = (min.z - origin.z) / direction.z;
		t1 = (max.z - origin.z) / direction.z;

		if (t1 < t0)
			std::swap(t0, t1);

		d0 = std::max(d0,t0);
		d1 = std::min(d1,t1);
	}

	// d1 < d0 || d0 == -INFINITY
	if (d1 < d0 || abs(d0 - INFINITY) < EPS)
		return false;

	return true;
}

Mesh::~Mesh()
{
	for (auto it = mFaces.begin(); it != mFaces.end(); ++it)
		delete *it;
}

RayIntersection Mesh::findIntersection(const Ray &ray) const
{
	if (! mAABB.intersects(ray))
		return RayIntersection(false);

	float closestDistance = INFINITY;
	RayIntersection closestIntersection(false);

	for (auto it = mFaces.begin(); it != mFaces.end(); ++it)
	{
		RayIntersection current = (*it)->findIntersection(ray);

		if (current.exists)
		{
			if (current.distance < closestDistance)
			{
				closestIntersection.exists	 = true;
				closestIntersection.distance = current.distance;
				closestIntersection.hitShape = this;
				closestIntersection.normal   = current.normal;

				closestDistance = current.distance;
			}
		}
	}

	return closestIntersection;
}

glm::vec3 Mesh::getNormal(const Ray &ray, float distance) const
{
	// This method never should be called!
	Q_ASSERT(! "This method never should be called!");

	return vec3(0);
}