#pragma once

#include "shape.hpp"
#include "triangle.hpp"

#include <vector>

struct AABB
{
	bool intersects(const Ray &ray) const;

	glm::vec3 min, max;
};


class Mesh : public BasicShape
{
	friend class SceneReader;
public:
	Mesh(const std::vector<Triangle *> &faces, const AABB &aabb,  Material *material)
		: BasicShape(material), mFaces(faces), mAABB(aabb) { }
	~Mesh();

public:
	RayIntersection findIntersection(const Ray &ray) const;
	glm::vec3 getNormal(const Ray &ray, float distance) const;

private:
	std::vector<Triangle *> mFaces;
	AABB mAABB;
};