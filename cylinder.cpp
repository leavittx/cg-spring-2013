#include "cylinder.hpp"
#include "auxiliary.hpp"

using namespace glm;

RayIntersection Cylinder::findIntersection(const Ray &ray) const
{
	const vec3 &origin = ray.origin(), &direction = ray.direction();

	vec3 originToBottom = toBottom(origin);

	vec3 u = direction - mAxis * (dot(direction, mAxis));
	vec3 v = originToBottom - mAxis * (dot(originToBottom, mAxis));

	// Solve equation c.x * x^2 + c.y * x + c.z = 0
	vec3 c;
	c.x = dot(u, u);
	vec2 roots;
	float distance = -1;

	if (abs(c.x) > EPS)
	{
		c.y = 2 * dot(u, v);
		c.z = dot(v, v) - mRadius * mRadius;

		int num_roots = solve_equation(c, roots, true);

		if (num_roots == 0)
			return RayIntersection(false);

		if (roots.x > 0)
		{
			vec3 tb = toBottom(ray.apply(roots.x)), tt = toTop(ray.apply(roots.x));
			if (dot(mAxis, tb) > 0 && dot(mAxis, tt) < 0)
				distance = roots.x;
		}

		if (roots.y > 0)
		{
			vec3 tb = toBottom(ray.apply(roots.y)), tt = toTop(ray.apply(roots.y));
			if (dot(mAxis, tb) > 0 && dot(mAxis, tt) < 0)
				if (distance < 0 || roots.y < distance)
					distance = roots.y;
		}
	}
	else
	{
		distance = -1;
	}

	RayIntersection res = RayIntersection(true);

	float axisDotDirection = dot(mAxis, direction);
	if (fabs(axisDotDirection) < EPS)
	{
		if (distance > 0)
		{
			res.distance = distance;
			res.hitShape = this;
			res.normal	 = getNormal(ray, distance);
			return res;
		}

		return RayIntersection(false);
	}

	// Bottom
	float axisDotOrigin = dot(mAxis, origin);
	float centerToOriginDotAxis = dot(originToBottom, mAxis);
	roots.x = -centerToOriginDotAxis / axisDotDirection;
	if (roots.x > 0)
	{
		vec3 tb = toBottom(ray.apply(roots.x));
		if (dot(tb, tb) < mRadius * mRadius)
			if (distance < 0 || roots.x < distance)
				distance = roots.x;
	}

	// Top
	float topCeneterToOriginDotAxis = dot(origin - mTopCenter, -mAxis);
	roots.x = topCeneterToOriginDotAxis / axisDotDirection;
	if (roots.x > 0)
	{
		vec3 tt = toTop(ray.apply(roots.x));
		if (dot(tt, tt) < mRadius * mRadius)
			if (distance < 0 || roots.x < distance)
				distance = roots.x;
	}

	if (distance > 0)
	{
		res.distance = distance;
		res.hitShape = this;
		res.normal	 = getNormal(ray, distance);
		return res;
	}

	return RayIntersection(false);
}

glm::vec3 Cylinder::getNormal(const Ray &ray, float distance) const
{
	vec3 p = ray.apply(distance); // Point of intersection

	// Bottom
	vec3 tb = toBottom(p);
	if (fabs(dot(mAxis, tb)) < EPS && dot(tb, tb) < mRadius * mRadius)
		return -mAxis;

  // Top
	vec3 tt = toTop(p);
	if (abs(dot(mAxis, tt)) < EPS && dot(tt, tt) < mRadius * mRadius)
		return mAxis;

	return normalize(p - mAxis * dot(tb, mAxis) - mBottomCenter);
}