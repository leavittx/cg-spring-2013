#pragma once

#include <glm/glm.hpp>

typedef glm::vec3 color3;

#define INFINITY		1e6f
#define EPS					1e-5f

struct ColorDesc
{
	color3		ambient;   // ambient lighting
	color3		diffuse;   // diffuse lighting
	color3		specular;  // specular lighting
	color3		emissive;  // emissive lighting
	float			shininess; // amount of specular lighting / specular power

	ColorDesc()
		: ambient(color3(0.0f, 0.0f, 0.0f)),
			diffuse(color3(1.0f, 0.0f, 0.0f)),
			specular(color3(1.0f, 0.0f, 0.0f)),
			emissive(color3(0.0f, 0.0f, 0.0f)),
			shininess(0)
	{ }
	ColorDesc(const color3 &ambient, const color3 &diffuse, const color3 &specular,
						float shininess = 0, const color3 &emissive = color3())
		: ambient(ambient),
			diffuse(diffuse),
			specular(specular),
			emissive(emissive),
			shininess(shininess)
	{ }
};

struct Material
{
	Material() { }
	virtual ~Material() {	}

	ColorDesc color;

	float refractionIndex;
	float	illumination;
	float	refraction;
	float	reflection;

	const color3& ambient() const { return color.ambient; }
	const color3& diffuse() const { return color.diffuse; }
	const color3& specular() const { return color.specular; }
	float shininess() const { return color.shininess; }
};