#pragma once

class AbstractIntersectionObject
{
public:
   AbstractIntersectionObject() {}
   virtual ~AbstractIntersectionObject() = 0;
};