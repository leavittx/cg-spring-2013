#include "raytracer.hpp"

#include "scene.hpp"
#include "camera.hpp"
#include "material.hpp"
#include "scenereader.hpp"

#include <QImage>

using namespace glm;

RayTracer::RayTracer(QObject *parent)
	: QObject(parent),
		mScene(nullptr),
		mCamera(nullptr)
{
}

void RayTracer::run(const ClParams &params)
{
	mScene = QSharedPointer<Scene>(new Scene);
	mCamera = QSharedPointer<Camera>(new Camera);

	SceneReader reader;
	bool ok = reader.read(params.scene, mScene, mCamera);

	if (! ok)
	{
		emit finished();
		return;
	}


	vec3 p20(0.27639, -0.85064, 0.44721);
	vec3 p00(-0.72484, 0.52386, 0.44710);
	vec3 p10(0, 0, 1);
	vec3 p11(-0.72360, -0.52572, 0.44721);

	vec3 eRight(p10 - p20);
	vec3 eHalfCenter((p11 - p10) * 0.5f);
	vec3 eLeft(p10 - p00);

	vec3 vLeft = normalize(-eLeft - eHalfCenter);
	vec3 vRight = normalize(-eRight - eHalfCenter);


	vLeft = normalize(p20 - (p10 + eHalfCenter));
	vRight = normalize(p00 - (p10 + eHalfCenter));

	vLeft = normalize(cross(p00 - p10, p10 - p11));
	vRight = normalize(cross(p20 - p10, p10 - p11));

	float angle = dot(vLeft, vRight);

	qDebug() << "Angle between incident faces: " << angle << acos(angle) << acos(angle) * (180.f / M_PI);




	mCamera->setRenderPlaneDimensions(params.resolution_x, params.resolution_y);
	QImage image = QImage(mCamera->getRenderPlaneWidth(), mCamera->getRenderPlaneHeight(), QImage::Format_RGB32);

	render(image.bits());

	image.save(params.output);

	emit finished();
}

void RayTracer::render(uchar *image)
{
	RayIntersection hitSomething;
	const int totalRays = mCamera->getRenderPlaneHeight() * mCamera->getRenderPlaneWidth();

	// Loop over pixels
	for (int j = 0; j < mCamera->getRenderPlaneHeight(); j += 1)
	{
		for (int i = 0; i < mCamera->getRenderPlaneWidth(); i += 1)
		{
			// Calculate primary ray
			Ray primaryRay = mCamera->calculatePrimaryRay(i, j);
			// Trace it
			color3 illumination = trace(primaryRay,
																	1.f/* mScene->getAir()->reflection*/,
																	mScene->getAir()->refractionIndex,
																	0,
																	hitSomething);

      //float gamma = 1 / 2.2f;
      float gamma = 1.0f;
      illumination.r = pow(illumination.r, gamma);
      illumination.g = pow(illumination.g, gamma);
      illumination.b = pow(illumination.b, gamma);

#define PACK_RGBA(r, g, b, a) (((a & 0xff) << 24) | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff));

			uchar r, g, b, a;
			r = std::min(255, int(illumination.r * 255));
			g = std::min(255, int(illumination.g * 255));
			b = std::min(255, int(illumination.b * 255));
			a = 255;

			*((int *)image + j * mCamera->getRenderPlaneWidth() + i) = PACK_RGBA(r, g, b, a);

			int traced = j * mCamera->getRenderPlaneWidth() + i;
			if (! (traced % 300000))
				fprintf(stderr, "%3.0f%c\r", float(traced) / totalRays * 100, '%');
		}
	}
}


inline vec3 reflect(const vec3 &direction, const vec3 &normal)
{
	return -1.f * (2 * dot(normal, direction) * normal - direction);
}

inline bool refract(const vec3 &direction, const vec3 &normal, float cosThetaIncedent, float cosThetaTransmitted, float nue, vec3 &t)
{
  if (cosThetaTransmitted > 0)
  {
    // Refraction ray direction
		t = normalize((nue * direction) - (nue * cosThetaIncedent - sqrt(cosThetaTransmitted)) * normal);
    //t = normalize(nue * (direction - normal * dot(direction, normal)) - normal * sqrt(cosThetaTransmitted));
    //t = (nue * direction) + (nue * cosThetaIncedent - sqrt(cosThetaTransmitted)) * normal;		 
    return true;
  }

  return false;
}


inline float fresnelDielectric(float cosi, float cost, float etai, float etat)
{
  float Rparl = ((etat * cosi) - (etai * cost)) /
                ((etat * cosi) + (etai * cost));
  float Rperp = ((etai * cosi) - (etat * cost)) /
                ((etai * cosi) + (etat * cost));
  return (Rparl * Rparl + Rperp * Rperp) * 0.5;
}

inline float fresnelConductor(float cosi, float eta, float k)
{
	float tmp = (eta*eta + k*k) * cosi*cosi;
	float Rparl2 = (tmp - (2.f * eta * cosi) + 1) /
								 (tmp + (2.f * eta * cosi) + 1);
	float tmp_f = eta*eta + k*k;
	float Rperp2 = (tmp_f - (2.f * eta * cosi) + cosi*cosi) /
								 (tmp_f + (2.f * eta * cosi) + cosi*cosi);
	return (Rparl2 + Rperp2) * 0.5;
}

// Schlick approximation of Fresnel coefficients
inline float schlick(float cosi, float cost, float etai, float etat)
{
	float R0 = ((etat - 1) * (etat - 1)) /
						 ((etat + 1) * (etat + 1));
	float nC = (1 - cosi);
	return R0 + (1 - R0) * nC * nC * nC * nC * nC;
}

// Schlick2 approximation of Fresnel coefficients
inline float schlick2(float cosi, float cost, float etai, float etat)
{
	float C;
	if (etai >= etat)
		C = cosi;
	else
		C = cost;
	float R0 = ((etai - etat) * (etai - etat)) /
						 ((etai + etat) * (etai + etat));
	float nC = (1 - C);
	return R0 + (1 - R0) * nC * nC * nC * nC * nC;
}


color3 RayTracer::trace(const Ray &primaryRay, float reflectionAmount, float srcRefractionIndex, int recursionDepth, RayIntersection &hitInside)
{
	RayIntersection hitTest;
	const int maxDepth = 4;
	const float minReflectionAmount = 5e-4f;
	color3 color(0), bgColor(0);

	// We shouldn't exceed maximum recursion depth value
	if (recursionDepth > maxDepth)
		return color;

  bool isReflected = recursionDepth > 0;

	// Find intersection with the nearest to the camera object
	RayIntersection hit = mScene->findIntersection(primaryRay, false);

	if (! hit.exists)
		return bgColor;
	hitInside = hit;

	// Define short names for frequently used variables
	const vec3 &direction = primaryRay.direction(), &normal = hit.normal;
	const Material *m = hit.hitShape->material();
	// Determine point of primary ray intersection with hit object
	vec3 pointOfIntersection = primaryRay.apply(hit.distance);


  //
  // Debug normals
  //
  //return color3(abs(normal.x), abs(normal.y), abs(normal.z));
	//
	// Flat shading
	//
	//return hit.hitShape->ambient() + hit.hitShape->diffuse();
	//
	// Blinn-Phong shading
	//
	color = mScene->calculateIllumination(primaryRay, hit.hitShape, hit.distance, hit.normal);

	if (isReflected)
		return color;

	//
	// Reflection
	//
  // Works only on reflective surfaces
	if (reflectionAmount < minReflectionAmount || m->reflection < EPS)
		return color;

	// Reflection ray direction
	const vec3 reflectionRayDir = reflect(direction, normal);
	// Reflection ray
	Ray reflectionRay(pointOfIntersection + float(1e-2) * reflectionRayDir, reflectionRayDir);
	// Trace it
	color3 reflected = trace(reflectionRay,
												 	 reflectionAmount * m->reflection,
													 srcRefractionIndex,
													 recursionDepth + 1,
													 hitTest) * reflectionAmount * m->reflection;

	//return reflected;

	//
	// Refraction
	//
  float DdotN = dot(direction, normal);
  float cosThetaIncident;

  if (DdotN < 0)
    cosThetaIncident = -DdotN;
  else
    cosThetaIncident = DdotN;

  float nueTransmitted = m->refractionIndex;
	float nueIncident = srcRefractionIndex;
  float cosThetaTransmitted = 1 - nueTransmitted * nueTransmitted * (1 - cosThetaIncident * cosThetaIncident);

  float R = fresnelDielectric(cosThetaIncident, cosThetaTransmitted, nueIncident, nueTransmitted);
	//float R = fresnelConductor(cosThetaIncident, nueTransmitted, 1.4);
	//float R = schlick2(cosThetaIncident, cosThetaTransmitted, nueIncident, nueTransmitted); 
  R = clamp(R, 0.f, 1.f);

	// Refraction ray direction
  vec3 refractionRayDir;
  bool refractionOccurs = refract(direction,
                                  normal,
                                  cosThetaIncident,
                                  cosThetaTransmitted,
                                  nueTransmitted / nueIncident,
                                  refractionRayDir);

	// Total internal reflection check one
	if (! refractionOccurs)
	{
		//return vec3(0);
		return clamp(color + R * reflected, color3(0), color3(1)); 
	}

	// Refraction ray
  Ray refractionRay(pointOfIntersection + float(1e-2) * refractionRayDir, refractionRayDir);
  // Trace it
  color3 refracted = trace(refractionRay,
                           reflectionAmount,
                           srcRefractionIndex,
                           recursionDepth + 1,
                           hitTest);

	// Total internal reflection check two
	if (! hitTest.exists)
	{
		//return vec3(0);
		return clamp(color + R * reflected, color3(0), color3(1)); 
	}

	// Beer�Lambert�Bouguer law
	color3 absorbance = -1.f * (m->diffuse()) * 0.15f * hitTest.distance;
	color3 transparency = color3(exp(absorbance.r), exp(absorbance.g), exp(absorbance.b));
	transparency = clamp(transparency, color3(0), color3(1));

	//return transparency * (1 - R) * refracted;
	color += (R * reflected + (1 - R) * refracted * transparency * m->refraction);

	return clamp(color, color3(0), color3(1));
}