#pragma once

#include "clparams.hpp"
#include "material.hpp"

#include <QtCore>

class		Scene;
class		Camera;
struct	Ray;
struct	RayIntersection;

// "Slow, but mostly perfect nature simulator"
class RayTracer : public QObject
{
	Q_OBJECT
public:
	RayTracer(QObject *parent = 0);

public slots:
	void run(const ClParams &parser);

signals:
	void finished();

private: 
	void render(uchar *image);
	color3 trace(const Ray &primaryRay, float reflectionAmount, float srcRefractionIndex, int recursionDepth, RayIntersection &hitInside);

private:
	QSharedPointer<Scene>  mScene;
	QSharedPointer<Camera> mCamera;
};