#include "light.hpp"
#include "ray.hpp"
#include "shape.hpp"

using namespace glm;

color3 Light::calculateIllumination(const Scene &scene, const BasicShape *object,
																		const Ray &primaryRay, float distanceView, const vec3 &normal) const
{
	// Point of primary ray with object intersection 
	vec3 pointOfIntersection = primaryRay.apply(distanceView);

	// Estimate light source attenuation factor in particular point
	vec3 distance_vector = m_position - pointOfIntersection;
	float attenuation = calculateAttenuation(distance_vector);

	// Modulate ambient color (by multiplication of components)
	color3 ambient = object->ambient() * m_color.ambient;

	// Shadow ray direction vector
	vec3 shadowRayDir;
	switch (m_type)
	{
	case LIGHT_DIRECTIONAL:
		shadowRayDir = -m_direction;
		break;
	case LIGHT_SPOT:
		shadowRayDir = -m_direction;
		break;
	case LIGHT_POINT:
		shadowRayDir = (m_position - pointOfIntersection);
		break;
	}

	// Shadow ray
	Ray shadowRay(pointOfIntersection + float(1e-3) * shadowRayDir, shadowRayDir);
	// Check if object point is occluded by something and therefore is in the shadow
	RayIntersection occlusion = scene.findIntersection(shadowRay, true);
	if (occlusion.exists && occlusion.distance < length(distance_vector))
		return ambient * attenuation;
	
  //
	// Diffuse calculations
  //
	// (N, L)
	float NdotL = dot(normal, shadowRay.direction()); 
	// Check orientation of normal
	if (NdotL <= 0)
		return ambient * attenuation;

	// Modulate diffuse color
	color3 diffuse = object->diffuse() * (m_color.diffuse * NdotL);

  //
	// Specular calculations
  //
	vec3 refl = normalize(2 * dot(shadowRay.direction(), normal) * normal - shadowRay.direction());
	vec3 view = normalize(primaryRay.origin() - pointOfIntersection);
	// (V, R)
	float VdotR = dot(view, refl);
	// Check orientation of reflected vector
	if (VdotR <= 0)
		return ambient * attenuation + diffuse * attenuation;

	// Modulate specular color
	color3 specular = object->specular() * (m_color.specular * pow(VdotR, object->shininess()));

	return ambient * attenuation + diffuse * attenuation + specular * attenuation;
}