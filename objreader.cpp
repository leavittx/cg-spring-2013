#include "objreader.hpp"

#include <QStringList>
#include <qdebug.h>

using namespace std;
using namespace glm;

vec3 read_vec3(const QString &line)
{
	QStringList coords = line.split(" ", QString::SkipEmptyParts);

	coords.removeAll(" ");
	coords.removeAll("");
	coords.removeAll("\n");
	coords.removeAll("\r");

	if (coords.size() == 3)
		return vec3(coords[0].toFloat(), coords[1].toFloat(), coords[2].toFloat());

	qDebug() << "Skipping";
	return vec3(0);
}

QStringList toIndicesDescriptor(const QString &line)
{
	QStringList	descs = line.split(" ", QString::SkipEmptyParts);
	return descs;
}

void toIndices(const QString &line, uint_t *position, uint_t *normal, uint_t *texcoord)
{
	QStringList indices = line.split("/");
	*position = indices[0].toUInt();
	*texcoord = indices[1].toUInt();
	*normal   = indices[2].toUInt();
}

bool ObjReader::read(MeshBB &meshBB, const QString &fileName, const glm::vec3 &translation, const glm::vec3 &scale)
{
	vector<Triangle *> &faces = meshBB.first;
	AABB &aabb = meshBB.second;

	// Read obj file and construct triangles
	QFile modelFile(fileName);

	modelFile.open(QIODevice::ReadOnly);
	if (! modelFile.isOpen())
	{
		qDebug() << "Failed loading model file: " << fileName;
		return false;
	}

	vector<vec3> positions;
	vector<vec3> normals;
	//vector<vec3> texCoords;

	vector<Vertex> faceVertices;
	vector<uint_t> faceVerticesIndices;
	vector<uint_t> faceIndices;

	// Result indices for triangles construction
	vector<uint_t> indices;
	// Result vertices for triangles construction
	vector<Vertex> vertices;

	while (true)
	{
		QString line = modelFile.readLine();
		if (modelFile.atEnd())
		{
			break;
		}

		if (line.contains("#"))
		{
			// Skip comment block
			continue;
		}
		else if (line.startsWith("v "))
		{
			// Vertex position
			vec3 pos = read_vec3(line.mid(2)) * scale + translation;
			positions.push_back(pos);
		}
		else if (line.startsWith("vt "))
		{
			// Skip texture coords
			//texCoords.push_back(read_vec3(line.mid(2)));
		}
		else if (line.startsWith("vn "))
		{
			// Vertex normal
			normals.push_back(read_vec3(line.mid(2)));
		}
		else if (line.startsWith("f "))
		{
			faceVertices.clear();
			faceVerticesIndices.clear();

			QStringList indicesDescs = toIndicesDescriptor(line.mid(2));

			foreach (const QString& index, indicesDescs)
			{
				uint_t position, texcoord, normal;
				Vertex v;

				toIndices(index, &position, &normal, &texcoord);

				// OBJ uses 1-based arrays
				if (! positions.empty())
					v.pos = positions[position - 1];
				if (! normals.empty())
					v.n = normals[normal - 1];
				//if (!texCoords.empty())
				//	v.TexCoords = texCoords[texcoord - 1];

				faceVertices.push_back(v);
				faceVerticesIndices.push_back(position);
			}

			uint_t count = faceVertices.size();
			faceIndices.resize(count);

			for (uint_t idx = 0; idx < count; ++idx)
			{
				// Triangle strip
				if (idx > 2)
				{
					indices.push_back( faceIndices[0] );
					indices.push_back( faceIndices[idx - 1] );
				}
				faceIndices[idx] = vertices.size();
				vertices.push_back(faceVertices[idx]);
				indices.push_back(faceIndices[idx]);
			}
		}
	}

	for (uint_t idx = 0; idx < indices.size(); idx += 3)
	{
		Vertex a = vertices[indices[idx + 0]];
		Vertex b = vertices[indices[idx + 1]];
		Vertex c = vertices[indices[idx + 2]];

		faces.push_back(new TriangleSmoothed(a.pos, b.pos, c.pos, a.n, b.n, c.n, nullptr));
	}

	// Form AABB
	vec3 minV, maxV;
	for (auto pos = positions.begin(); pos != positions.end(); ++pos)
	{
		const glm::vec3 &position = *pos;

		// Scan for min
		if (position.x < minV.x)
			minV.x = position.x;
		if (position.y < minV.y)
			minV.y = position.y;
		if (position.z < minV.z)
			minV.z = position.z;

		// Scan for max
		if (position.x > maxV.x)
			maxV.x = position.x;
		if (position.y > maxV.y)
			maxV.y = position.y;
		if (position.z > maxV.z)
			maxV.z = position.z;
	}

	aabb.min = minV;
	aabb.max = maxV;

	return true;
}