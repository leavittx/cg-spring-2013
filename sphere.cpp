#include "sphere.hpp"
#include "auxiliary.hpp"

using namespace glm;

RayIntersection Sphere::findIntersection(const Ray &ray) const
{
	vec3 centerToOrigin = ray.origin() - mCenter;

	// Solve equation x^2 + c.y * x + c.z = 0
	vec3 c;
	c.x = 1;
	c.y = dot(ray.direction(), centerToOrigin);
	c.z = dot(centerToOrigin, centerToOrigin) - mRadius * mRadius;
	vec2 roots;
	int num_roots = solve_equation(c, roots);

	if (num_roots == 0)
		return RayIntersection(false);

	float	distance = closest(roots);

	if (distance > 0)
	{
		RayIntersection result(true);
		result.distance = distance;
		result.hitShape = this;
		result.normal	 = getNormal(ray, distance);
		return result;
	}

	return RayIntersection(false);
}

vec3 Sphere::getNormal(const Ray &ray, float distance) const
{
	vec3 normal = (ray.apply(distance) - mCenter) / mRadius;
	return normalize(normal);
}