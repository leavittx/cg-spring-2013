#pragma once

#include "material.hpp"
#include "scene.hpp"

enum LightType {
	LIGHT_DIRECTIONAL = 0,
	LIGHT_SPOT        = 1,
	LIGHT_POINT				= 2
};

class Light
{
public:
	Light() { }
	virtual ~Light() { }

public:
	LightType getType() const { return m_type; }
	void      setType(LightType t) { m_type = t; }

	const ColorDesc& getColor() const { return m_color; }
	void  setColor(const ColorDesc &color) { m_color = color; }

	const glm::vec3& getPosition() const { return m_position; }
	void  setPosition(const glm::vec3 &p) { m_position = p; }

	const glm::vec3& getDirection() const { return m_direction; }
	void  setDirection(const glm::vec3 &d) { m_direction = glm::normalize(d); }

	const glm::vec3& getAttenuation() const { return m_attenuation; }
	void  setAttenuation(const glm::vec3 &t) { m_attenuation = t; }

	float getOuterCutoff() const { return m_outerCutoff; }
	void  setOuterCutoff(float s) { m_outerCutoff = cos(s * 0.5f * (180.f / 3.1415926535f)); }

	float getInnerCutoff() const { return m_innerCutoff; }
	void  setInnerCutoff(float s) { m_innerCutoff = cos(s * 0.5f * (180.f / 3.1415926535f)); }

	float getExponent() const { return m_exponent; }
	void  setExponent(float s) { m_exponent = s; }

public:
	color3 calculateIllumination(const Scene &scene, const BasicShape *object,
															 const Ray &primaryRay, float distanceView, const glm::vec3 &normal) const;

private:
	LightType m_type;
	ColorDesc m_color;

	glm::vec3  m_position;
	glm::vec3  m_direction;
	glm::vec3  m_attenuation;

	float m_outerCutoff;
	float m_innerCutoff;
	float m_exponent; /// Spot power

private:
	inline float calculateAttenuation(const glm::vec3 &distance_vector) const
	{
		// Distance to light source
		float distance = glm::length(distance_vector);
		// Distance attenuation
		float att1 = 1.f / (m_attenuation.x + m_attenuation.y * distance + m_attenuation.z * distance * distance), att2;

		switch (m_type)	{
		case LIGHT_DIRECTIONAL:
			return 1.f;
		case LIGHT_POINT:
			return glm::clamp(att1, 0.f, 1.f);;
		case LIGHT_SPOT:
			break;
		}

		float angle = glm::dot(glm::normalize(distance_vector), -m_direction);

		if (angle > m_outerCutoff)
			att2 = 1;
		else if (angle < m_innerCutoff)
			att2 = 0;
		else
		{
			//float factor = (acos(rho) - acos(m_innerCutoff)) / (acos(m_outerCutoff) - acos(m_innerCutoff));
			float factor = (angle - m_innerCutoff) / (m_outerCutoff - m_innerCutoff);
			att2 = pow(factor, m_exponent);
		}

		return glm::clamp(att1 * att2, 0.f, 1.f);
	}
};