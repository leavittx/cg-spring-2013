#include "cone.hpp"
#include "auxiliary.hpp"

using namespace glm;

RayIntersection Cone::findIntersection(const Ray &ray) const
{
	const vec3 &origin = ray.origin(), &direction = ray.direction();

	vec3 originToBottom = toBottom(origin);

	float dirDotAxis = dot(direction, mAxis);
	float CODotAxis	 = dot(originToBottom, mAxis);

	vec3 u = direction + mAxis * (-dirDotAxis);
	vec3 v = originToBottom + mAxis * (-CODotAxis);
	float w = CODotAxis * mRadiusDecrease;

	float	radPerDir = dirDotAxis * mRadiusDecrease;

	// Solve equation c.x * x^2 + c.y * x + c.z = 0
	vec3 c;
	c.x = dot(u, u) - radPerDir * radPerDir;
	vec2 roots;
	float distance = -1;
	float rayExit = -1.f; // Distance, where ray has exited the cone
	if (abs(c.x) > EPS)
	{
		c.y = 2 * (dot(u, v) - w * radPerDir);
		c.z = dot(v, v) - w * w;

		int num_roots = solve_equation(c, roots, true);

		if (num_roots == 0)
			return RayIntersection(false);

		if (roots.x > 0)
		{
			vec3 tb = toBottom(ray.apply(roots.x)), tt = toTop(ray.apply(roots.x));
			if (dot(mAxis, tb) > 0 && dot((-mAxis), tt) > 0)
				distance = roots.x;
		}
		if (roots.y > 0.f)
		{
			vec3 tb = toBottom(ray.apply(roots.y)), tt = toTop(ray.apply(roots.y));
			if (dot(mAxis, tb) > 0 && dot(-mAxis, tt) > 0)
				if (distance < 0 || roots.y < distance)
					distance = roots.y;
		}
	}


	RayIntersection res = RayIntersection(true);

	if (abs(dirDotAxis) < EPS)
	{
		if (distance > 0)
		{
			res.distance = distance;
			res.hitShape = this;
			res.normal	 = getNormal(ray, distance);
			return res;
		}

		return RayIntersection(false);
	}

	roots.x = (dot(-mAxis, toTop(origin))) / dirDotAxis;
	if (roots.x > 0)
	{
		vec3 tt = toTop(ray.apply(roots.x));
		if (dot(tt, tt) < mRadius * mRadius)
		{
			if (distance < 0 || roots.x < distance)
			{
				distance = roots.x;
			}
		}
	}

	if (distance > 0)
	{
		res.distance = distance;
		res.hitShape = this;
		res.normal	 = getNormal(ray, distance);
		return res;
	}

	return RayIntersection(false);
}


vec3 Cone::getNormal(const Ray &ray, float distance) const
{
	vec3 p = ray.apply(distance); // Point of intersection
	vec3 tb = toTop(p);
	if (abs(dot(mAxis, tb)) < EPS && dot(tb, tb) < mRadius * mRadius)
	{
		return mAxis;
	}
	vec3 approxNorm = p - (mAxis * (dot(toTop(p), mAxis)) + mTopCenter);
	return normalize(approxNorm + mAxis * (-mRadiusDecrease * length(approxNorm)));
}
