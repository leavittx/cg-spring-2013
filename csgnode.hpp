#pragma once

#include "shape.hpp"

namespace CSG {

class Node : public BasicShape
{
public:
	Node();
	virtual ~Node();
};

class LeafNode : public Node
{
public:
	LeafNode(BasicShape *shape)
		: mShape(shape) { }
	~LeafNode();

	virtual RayIntersection findIntersection( const Ray &ray ) const;
	virtual glm::vec3 getNormal( const Ray &ray, float distance ) const;

private:
	BasicShape *mShape;
};

class OpNode : public Node
{
public:
	OpNode(Node *l, Node *r)
		: mLeft(l), mRight(r) {	}
	~OpNode();

protected:
	RayIntersection getFarIntersection(RayIntersection nearIsect, Node *node, const Ray &ray) const;
	Node *mLeft, *mRight;
};

} // namespace CSG