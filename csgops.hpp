#pragma once

#include "csgnode.hpp"

namespace CSG {

class Union : public OpNode
{
public:
	Union(Node *l, Node *r)
		: OpNode(l, r) { } 
	~Union() { }

public:
	virtual RayIntersection findIntersection( const Ray &ray ) const;
	virtual glm::vec3 getNormal( const Ray &ray, float distance ) const;
};

class Intersection : public OpNode
{
public:
	Intersection(Node *l, Node *r)
		: OpNode(l, r) { }
	~Intersection() { }

public:
	virtual RayIntersection findIntersection( const Ray &ray ) const;
	virtual glm::vec3 getNormal( const Ray &ray, float distance ) const;
};

class Difference : public OpNode
{
public:
	Difference(Node *l, Node *r)
		: OpNode(l, r) { }
	~Difference()  { }

public:
	virtual RayIntersection findIntersection( const Ray &ray ) const;
	virtual glm::vec3 getNormal( const Ray &ray, float distance ) const;
};

} // namespace CSG