#pragma once

#include "material.hpp"
#include "ray.hpp"

#include <glm/glm.hpp>

class BasicShape;

class RayIntersection
{
public:
	RayIntersection(bool exists_ = false)
		: exists(exists_), pFar(nullptr) { }

	bool exists;
	float distance;
	const BasicShape *hitShape;
	glm::vec3 normal;

	RayIntersection *pFar;
};


class BasicShape
{
public:
	BasicShape() { }
	BasicShape(Material *material) : mMaterial(material) { }

	virtual ~BasicShape()
	{
		delete mMaterial;
	}

public:
	virtual RayIntersection findIntersection(const Ray &ray) const = 0;
	virtual glm::vec3 getNormal(const Ray &ray, float distance) const = 0;

	const Material* material() const { return mMaterial; };
	const color3& ambient() const { return mMaterial->ambient(); }
	const color3& diffuse() const { return mMaterial->diffuse(); }
	const color3& specular() const { return mMaterial->specular(); }
	float shininess() const { return mMaterial->shininess(); }

	float reflection() const { return mMaterial->reflection; }
	float refraction() const { return mMaterial->refraction; }
	float refractionIndex() const { return mMaterial->refractionIndex; }

protected:
	Material *mMaterial;
};