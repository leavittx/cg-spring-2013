#include "csgops.hpp"
#include "rothdiagram.hpp"

using namespace CSG;
using namespace glm;

//////////////////////////////////////////////////////////////////////////
/// Union
//////////////////////////////////////////////////////////////////////////
RayIntersection Union::findIntersection( const Ray &ray ) const
{
	RayIntersection pAmin = mLeft->findIntersection(ray);
	RayIntersection pBmin = mRight->findIntersection(ray);

	if (pAmin.exists && ! pBmin.exists)
		return pAmin;
	else if (pBmin.exists && ! pAmin.exists)
		 return pBmin;
	else if (pBmin.exists && pAmin.exists)
	{
		if (pAmin.distance < pBmin.distance)
			return pAmin;
		else
			return pBmin;
	}

	return RayIntersection(false);
}

glm::vec3 Union::getNormal( const Ray &ray, float distance ) const
{
	return vec3(0);
}

//////////////////////////////////////////////////////////////////////////
/// Intersection
//////////////////////////////////////////////////////////////////////////
RayIntersection Intersection::findIntersection( const Ray &ray ) const
{
	RayIntersection pAmin  = mLeft->findIntersection(ray);
	RayIntersection pBmin = mRight->findIntersection(ray);

	if (pAmin.exists && pBmin.exists)
	{
		RayIntersection pAfar = getFarIntersection(pAmin, mLeft, ray);
		RayIntersection pBfar = getFarIntersection(pBmin, mRight, ray);

		if (pAmin.distance < pBmin.distance && pAfar.distance > pBmin.distance)
		{
			return pBmin;
		}
		else if (pBmin.distance < pAmin.distance && pBfar.distance > pAmin.distance)
		{
			return pAmin;
		}
	}

	return RayIntersection(false);
}

glm::vec3 Intersection::getNormal( const Ray &ray, float distance ) const
{
	return vec3(0);
}

//////////////////////////////////////////////////////////////////////////
/// Difference: Right \ Left
//////////////////////////////////////////////////////////////////////////
RayIntersection Difference::findIntersection( const Ray &ray ) const
{
	RayIntersection pAmin = mLeft->findIntersection(ray);
	RayIntersection pBmin = mRight->findIntersection(ray);

	if (! pAmin.exists)
		return RayIntersection(false);

	if (! pBmin.exists)
		return pAmin;

	RayIntersection pAfar = getFarIntersection(pAmin, mLeft, ray);
	RayIntersection pBfar = getFarIntersection(pBmin, mRight, ray);

	if (pBfar.distance < pAmin.distance)
		return pAmin;

	if (pAfar.distance < pBmin.distance)
		return pAmin;

	if (pBfar.distance < pAfar.distance)
	{
		pBfar.normal *= -1;
		return pBfar;
	}

	return RayIntersection(false);
}

glm::vec3 Difference::getNormal( const Ray &ray, float distance ) const
{
	return vec3(0);
}