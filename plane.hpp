#pragma once

#include "shape.hpp"

class Plane : public BasicShape
{
	friend class SceneReader;
public:
	Plane(const glm::vec3 &normal, float distance, Material *material)
		: BasicShape(material),	mNormal(normal), mDistance(distance) { }
	~Plane() { }

public:
	RayIntersection findIntersection(const Ray &ray) const;
	glm::vec3 getNormal(const Ray &ray, float distance) const;

private:
	glm::vec3 mNormal;
	float			mDistance;
};