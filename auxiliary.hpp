#pragma once

#include <glm/glm.hpp>

inline int solve_equation(const glm::vec3 &c, glm::vec2 &roots, bool straightforward = false)
{
	float D, divider;

	if (straightforward)	
		D = c.y * c.y - 4.f * c.x * c.z;
	else
		D = c.y * c.y - c.x * c.z;

	if (D < 0)
		return 0;

	D = sqrt(D);

	if (straightforward)	
		divider = 1.f / (2 * c.x);
	else
		divider = 1.f / c.x;

	roots.x = (-c.y - D) * divider;
	roots.y = (-c.y + D) * divider;

	// D = 0
	if (abs(D) < EPS)
		return 1;

	return 2;
}

// finds closest to zero positive root
inline float closest(const glm::vec2 &r)
{
	float closest = INFINITY;

	//closest = max(roots.x, 0.f), max(roots.y, 0)

	if (r.x > 0)
		closest = r.x;

	if (r.y > 0)
		if (r.y < closest)
			closest = r.y;

	return closest;
}