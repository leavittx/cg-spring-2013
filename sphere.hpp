#pragma once

#include "shape.hpp"

class Sphere : public BasicShape
{
	friend class SceneReader;
public:
	Sphere(const glm::vec3 &center, float radius, Material *material)
		: BasicShape(material), mCenter(center),  mRadius(radius) { }
	~Sphere() { }

public:
	RayIntersection findIntersection(const Ray &ray) const;
	glm::vec3 getNormal(const Ray &ray, float distance) const;

private:
	glm::vec3 mCenter;
	float			mRadius;
};