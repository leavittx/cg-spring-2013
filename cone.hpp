#pragma once

#include "shape.hpp"

class Cone : public BasicShape
{
	friend class SceneReader;
public:
	Cone(const glm::vec3 &bottomC, const glm::vec3 &topC, float r, Material *material)
		: BasicShape(material), mBottomCenter(bottomC), mTopCenter(topC), mRadius(r),
			mAxis(glm::normalize(mTopCenter - mBottomCenter)),
			mRadiusDecrease(mRadius / glm::length(mTopCenter - mBottomCenter)) { }
	~Cone() { }

public:
	RayIntersection findIntersection(const Ray &ray) const;
	glm::vec3 getNormal(const Ray &ray, float distance) const;

private:
	inline glm::vec3 toBottom(const glm::vec3 &v) const
	{
		return v - mBottomCenter;
	}

	inline glm::vec3 toTop(const glm::vec3 &v) const
	{
		return v - mTopCenter;
	}

private:
	glm::vec3 mBottomCenter, mTopCenter;
	float mRadius;
	glm::vec3 mAxis;
	float mRadiusDecrease;
};