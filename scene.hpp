#pragma once

#include "material.hpp"

#include <vector>


class		BasicShape;
class		Light;
struct	Ray;
struct	RayIntersection;

class Scene
{
public:
	Scene();
	virtual ~Scene();

	RayIntersection findIntersection(const Ray &ray, bool any) const;
	color3 calculateIllumination(const Ray &primaryRay, const BasicShape *object, float distance, const glm::vec3 &normal) const;

	void addObject(BasicShape *object);
	void addLight(Light *light);
	void clear();

	Material* const getAir() const { return mAir; }
	void setAir(Material* material);

private:
	std::vector<BasicShape *> mObjects;
	std::vector<Light *> mLights;
	Material *mAir;
};