#pragma once

#include <glm/glm.hpp>

struct Ray
{
public:
	// Constructs a ray
	Ray(const glm::vec3& mOrigin = glm::vec3(), const glm::vec3& direction = glm::vec3())
		: mOrigin(mOrigin), mDirection(glm::normalize(direction))
	{
	}
	virtual ~Ray() { }

	// Gives position on the ray at given distance
	// R = O + D * t - canonical ray formulae
	glm::vec3 apply(float t) const
	{
		return mOrigin + mDirection * t;
	}

	const glm::vec3& origin() const { return mOrigin; }
	const glm::vec3& direction() const { return mDirection; }

private:
	glm::vec3 mOrigin;
	glm::vec3 mDirection;
};