#pragma once

#include "mesh.hpp"

#include <glm/glm.hpp>
#include <QFile>
#include <QPair>
#include <vector>

struct Vertex 
{
	glm::vec3 pos;
	glm::vec3 n;
};

typedef QPair< std::vector<Triangle *>, AABB > MeshBB;

class ObjReader
{
public:
	ObjReader() { }

	bool read(MeshBB &meshBB, const QString &fileName, const glm::vec3 &translation, const glm::vec3 &scale);
};