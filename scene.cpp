#include "scene.hpp"

#include "light.hpp"
#include "material.hpp"
#include "shape.hpp"
#include "camera.hpp"

Scene::Scene() : mAir(nullptr)
{
}

Scene::~Scene()
{
	clear();
}

RayIntersection Scene::findIntersection(const Ray &ray, bool any) const
{
	const BasicShape *nearestThing;
	float nearestThingDistance = INFINITY;
	RayIntersection nearestThingIntersection(false);

	for (auto it = mObjects.begin(); it != mObjects.end(); ++it)
	{
		const BasicShape *shape = *it;
		RayIntersection hit = shape->findIntersection(ray);

		if (! hit.exists)
			continue;

		if (hit.distance < nearestThingDistance)
		{
			nearestThingDistance = hit.distance;
			nearestThing = shape;
			nearestThingIntersection = hit;
		}

		if (any)
			break;
	}

	return nearestThingIntersection;
}

color3 Scene::calculateIllumination(const Ray &primaryRay, const BasicShape *object, float distance, const glm::vec3 &normal) const
{
	color3 color;

	// Light is additive, so we accumulate the light intensity derived from different sources
	for (auto l = mLights.begin(); l != mLights.end(); ++l)
	{
		color += (*l)->calculateIllumination(*this, object, primaryRay, distance, normal);
	}

	return color;
}

void Scene::addObject(BasicShape *object)
{
	mObjects.push_back(object);
}

void Scene::addLight(Light *light)
{
	mLights.push_back(light);
}

void Scene::setAir(Material* material)
{
	if (mAir)
		delete mAir;

	mAir = material;
}

void Scene::clear()
{
	for (auto it = mObjects.begin(); it != mObjects.end(); ++it)
		delete *it;
	mObjects.clear();

	for (auto l = mLights.begin(); l != mLights.end(); ++l)
		delete *l;
	mLights.clear();

	delete mAir;
}