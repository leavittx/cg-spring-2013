#pragma once

#include "shape.hpp"

class Triangle : public BasicShape
{
	friend class SceneReader;
public:
	Triangle(const glm::vec3 &a, const glm::vec3 &b, const glm::vec3 &c, Material *material)
		: BasicShape(material),	mPointA(a), mPointB(b), mPointC(c),
			mNormal(glm::cross(mPointB - mPointA, mPointC - mPointA)) { }
	~Triangle() { }

public:
	RayIntersection findIntersection(const Ray &ray) const;
	glm::vec3 getNormal(const Ray &ray, float distance) const;

protected:
	glm::vec3 mPointA, mPointB, mPointC;
	glm::vec3 mNormal;
	mutable float mS, mT;
};

class TriangleSmoothed : public Triangle
{
	friend class SceneReader;
public:
	TriangleSmoothed(const glm::vec3 &a, const glm::vec3 &b, const glm::vec3 &c,
								 const glm::vec3 &na, const glm::vec3 &nb, const glm::vec3 &nc,
								 Material *material)
		: Triangle(a, b, c, material),
			mNormalA(na), mNormalB(nb), mNormalC(nc) { }
	~TriangleSmoothed() { }

public:
	RayIntersection findIntersection(const Ray &ray) const;

protected:
	glm::vec3 mNormalA, mNormalB, mNormalC;
};