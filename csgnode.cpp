#include "csgnode.hpp"

using namespace CSG;

Node::Node()
{
}

Node::~Node()
{
}

OpNode::~OpNode()
{
}

RayIntersection OpNode::getFarIntersection(RayIntersection nearIsect, Node *node, const Ray &ray) const
{
	RayIntersection farIsect = nearIsect;
	RayIntersection far(false);
	float dist = nearIsect.distance;
		
	Ray newRay = Ray(ray.apply(farIsect.distance) + 3.0f * 1e-4f * ray.direction(), ray.direction());
	farIsect = node->findIntersection(newRay);

	if (farIsect.exists)
	{
		far = farIsect;
		dist += farIsect.distance;
	}

	if (far.exists)
		far.distance = dist;
	else
		far.distance = INFINITY;

	return far;
}

LeafNode::~LeafNode()
{
}

RayIntersection LeafNode::findIntersection( const Ray &ray ) const
{
	return mShape->findIntersection(ray);
}

glm::vec3 LeafNode::getNormal( const Ray &ray, float distance ) const
{
	return glm::vec3(0);
}