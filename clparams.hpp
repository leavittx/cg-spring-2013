#pragma once

#include <QStringList>
#include <qdebug.h>

class ClParams
{
public:
   // my_ray_tracing.exe --scene=my_scene.xml --resolution_x=30 --resolution_y=42 --output=my_scene.png
   ClParams() {}
   ClParams(QStringList args)
   {
	  QRegExp rxArgScene("--scene=(\\S+)");
	  QRegExp rxArgResX("--resolution_x=(\\d+)");
	  QRegExp rxArgResY("--resolution_y=(\\d+)");
	  QRegExp rxArgOutput("--output=(\\S+)");

	  for (int i = 1; i < args.size(); ++i)
	  {
		 if (rxArgScene.indexIn(args.at(i)) != -1 ) {   
			scene = rxArgScene.cap(1);
			qDebug() << i << ": Scene: " << scene;
		 }
		 else if (rxArgResX.indexIn(args.at(i)) != -1 ) {   
			resolution_x = rxArgResX.cap(1).toInt();
			qDebug() << i << ": ResolutionX: " << resolution_x;
		 } 
		 else if (rxArgResY.indexIn(args.at(i)) != -1 ) {
			resolution_y = rxArgResY.cap(1).toInt();
			qDebug() << i << ": ResolutionY: " << resolution_y;
		 } 
		 else if (rxArgOutput.indexIn(args.at(i)) != -1 ) {
			output = rxArgOutput.cap(1);
			qDebug() << i << ": Output: " << output;
		 } 
		 else {
			qDebug() << "Unknown arg: " << args.at(i);
		 }
	  }
   }

   QString scene;
   int resolution_x, resolution_y;
   QString output;
};