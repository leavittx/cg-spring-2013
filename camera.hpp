#pragma once

#include "ray.hpp"

class Camera
{
private:
	// Camera coordinate system
	glm::vec3 mPosition; // Eye vector
	glm::vec3 mUp;       // Up direction vector
	glm::vec3 mLook;     // Look at vector

	glm::vec3 mRight;    // Right direction

	float	mAspectRatio;
	float	mFoV;
	float	mNearPlane;
	float mFocusDistance;

	int	mRenderPlaneWidth;
	int	mRenderPlaneHeight;

public:
	Camera();
	virtual ~Camera();

public:
	// Look at function
	void lookAt(const glm::vec3 &pos, const glm::vec3 &up, const glm::vec3 &look);

	// Set camera field of view
	void setFoV(float fov);  

	// Set camera near plane distance
	void setNearPlane(float distance);

protected:
	// Set camera position
	void setPosition(const glm::vec3 *Pos);
	void setPosition(const glm::vec3 &Pos);
	// Set camera up
	void setUp(const glm::vec3 *Up);
	void setUp(const glm::vec3 &Up);
	// Set camera look
	void setLook(const glm::vec3 *Look);
	void setLook(const glm::vec3 &Look);

	// Set camera right
	void setRight(const glm::vec3 *Right);
	void setRight(const glm::vec3 &Right);

	// Get camera position
	const glm::vec3& getPosition() const { return mPosition; }
	// Get up vector
	const glm::vec3& getUp() const { return mUp; }
	// Get look vector
	const glm::vec3& getLook() const { return mLook; }

	// Get right vector
	const glm::vec3& getRight() const { return mRight; }

public:
	// Ray tracing specific routines
	Ray calculatePrimaryRay(int x, int y) const;

	void setRenderPlaneDimensions(int w, int h);
	int getRenderPlaneWidth() const { return mRenderPlaneWidth; }
	int getRenderPlaneHeight() const { return mRenderPlaneHeight; }

	const glm::vec3& getX() const;
	const glm::vec3& getY() const;
	const glm::vec3& getZ() const;
};
