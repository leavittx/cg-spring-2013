#include <QtCore/QCoreApplication>
#include <QStringList>

#include "raytracer.hpp"
#include "clparams.hpp"

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);

	QStringList arguments = app.arguments();
	ClParams params(arguments);

	// Task parented to the application so that it
	// will be deleted by the application.
	RayTracer *tracer = new RayTracer(&app);
	tracer->run(params);


  //system("my_scene.png");

	// This will cause the application to exit when
	// the task signals finished.    
	//QObject::connect(tracer, SIGNAL(finished()), &app, SLOT(quit()));

	// This will run the task from the application event loop.
	//QTimer::singleShot(0, tracer, SLOT(run(const ArgsParser &parser)));

	//return app.exec();

	return 0;
}
