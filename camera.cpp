#include "camera.hpp"

#define _USE_MATH_DEFINES
#include <math.h>

using namespace glm;

Camera::Camera()
{
}

Camera::~Camera()
{
}

void Camera::lookAt(const vec3 &pos, const vec3 &up, const vec3 &look)
{
	// Transform into world coordinates
	// Set look vector
	setLook(normalize(look - pos));
	// Keep axes orthonormal
	setRight(normalize(cross(mLook, up)));
	setUp(cross(mRight, mLook));
	
	setPosition(pos);
}

void Camera::setFoV(float fov)
{
	mFoV = fov;
	float angle = mFoV * float(M_PI) / 180.f;
	mFocusDistance  = 1 / tan(angle * 0.5f);
}

void Camera::setNearPlane(float distance)
{
	mNearPlane = distance;
}

void Camera::setRenderPlaneDimensions(int w, int h)
{
	mRenderPlaneWidth = w;
	mRenderPlaneHeight = h;

	mAspectRatio = float(mRenderPlaneWidth) / mRenderPlaneHeight;
}

Ray Camera::calculatePrimaryRay(int x, int y) const
{
	float projX	= (2 * (float(x) / mRenderPlaneWidth - 0.5f) * mAspectRatio);
	float projY = (2 * (0.5f - float(y) / mRenderPlaneHeight));
	vec3 origin = getPosition();
	vec3 direction = getX() * projX + getY() * projY + getZ() * mFocusDistance;
	return Ray(origin, direction);
}

const vec3& Camera::getX() const
{
	return getRight();
}

const vec3& Camera::getY() const
{
	return getUp();
}

const vec3& Camera::getZ() const
{
	return getLook();
}

void Camera::setPosition(const vec3 *Pos)
{
	mPosition = *Pos;
}

void Camera::setPosition(const vec3 &Pos)
{
	setPosition(&Pos);
}

void Camera::setLook(const vec3 *Look)
{
	mLook = *Look;
}

void Camera::setLook(const vec3 &Look)
{
	setLook(&Look);
}

void Camera::setRight(const vec3 *Right)
{
	mRight = *Right;
}

void Camera::setRight(const vec3 &Right)
{
	setRight(&Right);
}

void Camera::setUp(const vec3 *Up)
{
	mUp = *Up;
}

void Camera::setUp(const vec3 &Up)
{
	setUp(&Up);
}